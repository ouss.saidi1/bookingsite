import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ImageModel } from 'src/app/model/ImageModel';

@Injectable({
  providedIn: 'root'
})
export class UploaderimageService {
  apiURL = 'http://localhost:8080';

  constructor(private http: HttpClient) 
  {

   }
   public upload(image:File): Observable<ImageModel>{
    let formParams = new FormData();
    formParams.append('file', image);
    
    return this.http.post<ImageModel>(this.apiURL + '/image' , formParams)
  }
  

}
