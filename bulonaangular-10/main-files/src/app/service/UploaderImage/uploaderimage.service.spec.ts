import { TestBed } from '@angular/core/testing';

import { UploaderimageService } from './uploaderimage.service';

describe('UploaderimageService', () => {
  let service: UploaderimageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UploaderimageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
