import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, retry, catchError } from 'rxjs';
import { ClientInscriptionDto } from '../model/ClientInscriptionDto';

@Injectable({
  providedIn: 'root'
})
export default class IsncriptionService {

  apiURL = 'http://localhost:8080';

  constructor(private http: HttpClient) { }


    // Http Options
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };


    inscription(client: ClientInscriptionDto): Observable<ClientInscriptionDto> {
      return this.http
        .post<ClientInscriptionDto>(
          this.apiURL + '/client',
          client,
          this.httpOptions
        );
    }
}
