import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  apiURL = 'http://localhost:8080';

  constructor(private http: HttpClient) { }




  login(userName: string , passWord:string): Observable<any> {
    var body = new URLSearchParams();
    body.set("username" , userName)
    body.set("password" , passWord)
  let headers = new HttpHeaders({'Content-Type' : 'application/x-www-form-urlencoded'})
    return this.http
      .post<any>(
        this.apiURL + '/login',
        body,{headers:headers}
      );
      
  }


}
