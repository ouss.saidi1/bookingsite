import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, retry, catchError } from 'rxjs';
import { Hotel } from '../model/hoteldto';

@Injectable({
  providedIn: 'root'
})
export class HotelService {
apiURL = 'http://localhost:8080';

  constructor(private http: HttpClient) 
  {

  }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  AddHotel(hotel: any): Observable<Hotel>
{
  return this.http.post<Hotel>
  ( this.apiURL + '/hotel',hotel, this.httpOptions ) 
}
}

// HttpClient API post() method => Create employee





