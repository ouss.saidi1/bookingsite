import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { data } from 'jquery';
import { Hotel } from 'src/app/model/hoteldto';
import { HotelService } from 'src/app/service/hotel.service';
import { UploaderimageService } from 'src/app/service/UploaderImage/uploaderimage.service';


@Component({
  selector: 'app-add-new-products',
  templateUrl: './add-new-products.component.html',
  styleUrls: ['./add-new-products.component.scss']
})
export class AddNewProductsComponent implements OnInit {
    ToAdd :Hotel;
    states=["option1","option2"];
    country:string;
    path : string;

    file: File = null;


    form: FormGroup = new FormGroup({
      nom: new FormControl(''),
      nbEtoiles: new FormControl(''),
      description: new FormControl(''),
      pays: new FormControl(''),
      ville: new FormControl(''),
      imagepath: new FormControl(false),
    });
    submitted = false;
  constructor(private formBuilder: FormBuilder, private hotelservice: HotelService,
    private imageService : UploaderimageService
    ) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        nom: ['',[Validators.required , Validators.min(4) ,Validators.max(30)]],
        nbEtoiles: ['',[Validators.required]],
        description: ['',[Validators.required , Validators.min(4) ,Validators.max(30)]],
        pays: ['',[Validators.required]],
        ville: ['',[Validators.required]],
      });
    $.getScript('./assets/plugins/Drag-And-Drop/imageuploadify.min.js');
    $.getScript("./assets/js/add-new-product-image-upload.js")
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    } 
  else 
  {
    this.ToAdd = {
      nom : this.f['nom'].value,
      nbEtoiles:this.f['nbEtoiles'].value,
      description:this.f['description'].value,
      pays:this.f['pays'].value,
      ville:this.f['ville'].value,
      imagepath:this.path
    };
    this.hotelservice.AddHotel(this.ToAdd).subscribe(res=> console.log(res));
    this.form.reset();
  }
  
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }
  setState()
  {
    if(this.country=="Maroc")
    
    {
      this.states=["Oujda","Ifrane","Marrakech","Rabat","Casablanca"];
    }
    if(this.country =="France")
    
    {
      this.states=["Lyon","Lile","Nice","Paris"];
    }
  }
  OnFileChange(event){
    console.log(event.target.files[0]);
    this.file = event.target.files[0];
    this.imageService.upload(this.file).subscribe(res => this.path = res.encodeBase64String);
  }


}
