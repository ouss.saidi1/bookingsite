import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtTokenInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if(localStorage.getItem('token') !=null){
      const requestWhitToken = request.clone({
        setHeaders:{
          Authorization: localStorage.getItem('token')
        }
      });
      return next.handle(requestWhitToken);
    }else return next.handle(request)


  }
}
