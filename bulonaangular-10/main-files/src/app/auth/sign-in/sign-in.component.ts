import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { LoginService } from 'src/app/service/login.service';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {


  form : FormGroup = new FormGroup({
    userName: new FormControl(''),
    password: new FormControl(''),
  });
  submited: boolean;
  
    constructor(private router: Router, private route: ActivatedRoute , private formBuilder:FormBuilder , private loginService :LoginService) { }

    // On Forgotpassword link click
    onForgotpassword() {
      this.router.navigate(['forgot-password'], { relativeTo: this.route.parent });
    }
  
    // On Signup link click
    onSignup() {
      this.router.navigate(['sign-up'], { relativeTo: this.route.parent });
    }
  

  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        userName: ['admin', Validators.required],
        password: ['admin', Validators.required],

      })
    
    }

    onSubmit(){
      this.submited = true;
      if(this.form.invalid) {
        console.log("no")
        return;
        }else{
          this.loginService.login(this.f['userName'].value, this.f['password'].value).subscribe(res => localStorage.setItem('token',res.accessToken))
        }

      }


      get f(): { [key: string]: AbstractControl } {
        return this.form.controls;
      }
}
