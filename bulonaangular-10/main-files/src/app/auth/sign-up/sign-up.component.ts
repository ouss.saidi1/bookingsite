import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import {ClientInscriptionDto} from 'src/app/model/ClientInscriptionDto';
import IsncriptionService from 'src/app/service/isncription.service';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  toAdd : ClientInscriptionDto;

  form : FormGroup = new FormGroup({
    fullname: new FormControl(''),
    userName: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
    confirmPassword: new FormControl(''),
    birthDate: new FormControl(),
  });

  submited : boolean = true;

  fullname: string;

  constructor(
     private router: Router,
     private route: ActivatedRoute,
     private formBuilder: FormBuilder,
     private isncriptionService : IsncriptionService
     ) { }

  // On Signup link click
  onSignIn() {
    this.router.navigate(['sign-in'], { relativeTo: this.route.parent });
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        fullName: ['', [Validators.required , Validators.min(4) ,Validators.max(30)]],
        userName: ['', [Validators.required, Validators.min(4)]],
        email: ['', [Validators.required , Validators.email]],
        password: ['', [Validators.required ,]],
        confirmPassword: ['', [Validators.required]],
        birthDate: ['',[Validators.required]],

      })

}


get f(): { [key: string]: AbstractControl } {
  return this.form.controls;
}

onSubmit(){
  this.submited = true;
  if(this.form.invalid) {
    console.log(this.form)
    return;
    }else{
      this.toAdd  = {
        fullName : this.f['fullName'].value,
        email : this.f['email'].value,
        password : this.f['password'].value,  
        confirmPassword :  this.f['confirmPassword'].value,  
        birthDate : this.f['birthDate'].value,  
        userName : this.f['userName'].value,  
        role : null
      }

      this.isncriptionService.inscription(this.toAdd).subscribe(res => {
        console.log(res)
      })
    }
}



}
