export interface Hotel {
    nom: string;
    nbEtoiles: number;
    description:string;
    pays: string;
    ville: string;
    imagepath:string;
 }
 