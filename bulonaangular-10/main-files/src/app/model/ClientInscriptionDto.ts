export interface ClientInscriptionDto {
    userName: string;
    email: string;
    password: string;
    confirmPassword : string
    role: string;
    fullName: string;
    birthDate: Date;
}