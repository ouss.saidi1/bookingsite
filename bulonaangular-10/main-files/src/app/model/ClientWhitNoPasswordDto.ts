export interface ClientWhitNoPasswordDto  {
    userName: string;
    email: string;
    fullName: string;
    birthDate: Date;
}