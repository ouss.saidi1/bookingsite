package com.example.demo.repository;

import com.example.demo.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Optional<Client> findByUsername(String userName);

    Optional<Client> findByUsernameAndPassword(String username, String password);

    boolean existsByUsername(String username);

}