package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Hotel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    private String nom;
    private int nbEtoiles;
    private String description ;
    private String pays;
    private String ville;
    @Column (columnDefinition = "Text")
    private String imagepath;

    @OneToMany(mappedBy = "hotel", orphanRemoval = true)
    private Set<Chambre> chambres = new LinkedHashSet<>();

}
