package com.example.demo.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor

public class ImageModel
{
    private String encodeBase64String;

    public ImageModel(String encodeBase64String) {
        this.encodeBase64String = encodeBase64String;
    }
}
