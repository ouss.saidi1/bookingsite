package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private Date dateDebut;
    private Date dateFin;
    private int nombresPersonne;

    @OneToMany(orphanRemoval = true)
    @JoinColumn(name = "reservation_id")
    private List<ChambreReservation> chambreReservations = new ArrayList<>();

}
