package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Client extends Personne {


    @Column(name="fullname")
    private String fullName;
    @Column(name="birth_date")
    private Date birthDate;

    public Client(Long id, String username, String email, String password, String role, String fullName, Date birthDate) {
        super(id, username, email, password, role);
        this.fullName = fullName;
        this.birthDate = birthDate;
    }
}
