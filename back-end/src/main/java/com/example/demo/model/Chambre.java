package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Chambre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String type;
    private int numChambre;
    private int numEtage;
    private float prix;

    @ManyToOne
    @JoinColumn(name = "type_chambre_id")
    private TypeChambre typeChambre;

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;

}
