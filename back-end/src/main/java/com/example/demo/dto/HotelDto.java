package com.example.demo.dto;

import com.example.demo.exception.validator.ValidationExceptionMessage;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Data
public class HotelDto implements Serializable {

    @NotBlank(message = ValidationExceptionMessage.NOT_BLANK)
    private  String nom;
    @NotBlank(message = ValidationExceptionMessage.NOT_BLANK)
    private int nbEtoiles;
    @Size(min = 2, message = ValidationExceptionMessage.TO_SHORT)
    @Size(max = 200, message = ValidationExceptionMessage.TO_LONG)
    private  String description;
    @NotBlank(message = ValidationExceptionMessage.NOT_BLANK)
    private  String pays;
    @NotBlank(message = ValidationExceptionMessage.NOT_BLANK)
    private  String ville;
    @NotBlank(message = ValidationExceptionMessage.NOT_BLANK)
    private  String imagepath;

}