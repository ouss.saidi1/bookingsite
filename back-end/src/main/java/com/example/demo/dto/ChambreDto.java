package com.example.demo.dto;

import com.example.demo.model.TypeChambre;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the {@link com.example.demo.model.Chambre} entity
 */
@Data
public class ChambreDto implements Serializable {

    private final String type;
    private final int numChambre;
    private final int numEtage;
    private final float prix;
    private final TypeChambre typeChambre;
    private final HotelDto hotel;
}