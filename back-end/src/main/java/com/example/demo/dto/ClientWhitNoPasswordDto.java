package com.example.demo.dto;

import com.example.demo.exception.validator.ValidationExceptionMessage;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Getter
@Setter
public class ClientWhitNoPasswordDto implements Serializable {
    @NotBlank(message = ValidationExceptionMessage.NOT_BLANK)
    private  String userName;

    @NotBlank(message = ValidationExceptionMessage.NOT_BLANK)
    @Email(message = ValidationExceptionMessage.NOT_VALID_EMAIL)
    private  String email;

    @Size(min = 2, message = ValidationExceptionMessage.TO_SHORT)
    @Size(max = 200, message = ValidationExceptionMessage.TO_LONG)
    private  String fullName;

    @NotNull
    @Past(message = ValidationExceptionMessage.NOT_VALID_DATE)
    private  Date birthDate;

}