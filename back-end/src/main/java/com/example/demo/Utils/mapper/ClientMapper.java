package com.example.demo.Utils.mapper;

import com.example.demo.dto.ClientInscriptionDto;
import com.example.demo.dto.ClientWhitNoPasswordDto;
import com.example.demo.model.Client;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.IoC;
import fr.xebia.extras.selma.Mapper;

@Mapper(withIgnoreMissing= IgnoreMissing.ALL , withIoC= IoC.SPRING)
public interface ClientMapper {
    Client asClient(ClientWhitNoPasswordDto clientWhitNoPasswordDto);
    Client asClient(ClientInscriptionDto in);
    ClientWhitNoPasswordDto asClientWhitNoPasswordDto(Client in);

}
