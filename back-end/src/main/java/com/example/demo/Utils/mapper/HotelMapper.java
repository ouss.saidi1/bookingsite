package com.example.demo.Utils.mapper;

import com.example.demo.dto.ClientWhitNoPasswordDto;
import com.example.demo.dto.HotelDto;
import com.example.demo.model.Client;
import com.example.demo.model.Hotel;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.IoC;
import fr.xebia.extras.selma.Mapper;

@Mapper(withIgnoreMissing= IgnoreMissing.ALL , withIoC= IoC.SPRING)
public interface HotelMapper {
    Hotel asHotel(HotelDto hotelDto);
    HotelDto asHotelDto(Hotel hotel);
}
