package com.example.demo.exception.hotel;

public class HotelException extends Exception {
    public HotelException(String message) {
        super(message);
    }
}
