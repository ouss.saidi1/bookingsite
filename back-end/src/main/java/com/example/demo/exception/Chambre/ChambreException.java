package com.example.demo.exception.Chambre;

public class ChambreException extends Exception {
    public ChambreException(String message) {
        super(message);
    }
}
