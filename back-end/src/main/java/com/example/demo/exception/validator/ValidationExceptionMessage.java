package com.example.demo.exception.validator;

public class ValidationExceptionMessage {

    public static final String NOT_VALID_EMAIL = "Invalid Email";
    public static final String NOT_BLANK = "Must not be Blank";

    public static final String TO_SHORT = "must not be short";

    public static final String TO_LONG = "must not be long";

    public static final String NOT_VALID_DATE = "Must be at lease 18 years old";

    public static final String NOT_VALID_PASSWORD = "The password's first character must be a letter, it must contain at least 4 characters and no more than 15 characters and no characters other than letters, numbers and the underscore may be used";

}
