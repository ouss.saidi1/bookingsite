package com.example.demo.exception.client;

public class ClientException extends Exception {
    public ClientException(String message){
        super(message);
    }

}
