package com.example.demo.propertyhandler;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource({
        "classpath:utils.properties"
})
public class PropertyHandler {
}
