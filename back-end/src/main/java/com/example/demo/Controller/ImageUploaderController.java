package com.example.demo.Controller;

import com.example.demo.model.ImageModel;
import com.example.demo.service.ImageUploaderService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@CrossOrigin("${front.end.url}")
@RestController
@RequestMapping("/image")
@AllArgsConstructor
public class ImageUploaderController {
    ImageUploaderService imageUploderService;
    @PostMapping
    public ImageModel convert(@RequestBody MultipartFile file){
        return imageUploderService.Convert(file);
    }

}
