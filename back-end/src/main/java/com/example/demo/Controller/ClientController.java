package com.example.demo.Controller;

import com.example.demo.dto.ClientInscriptionDto;
import com.example.demo.dto.ClientWhitNoPasswordDto;
import com.example.demo.exception.client.ClientException;
import com.example.demo.service.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/client")
@AllArgsConstructor
@CrossOrigin(origins = "${front.end.url}")
public class ClientController {
    private ClientService clientService;

    @GetMapping
    public ResponseEntity<List<ClientWhitNoPasswordDto>> getAllClient() throws ClientException {
        return new ResponseEntity<>(clientService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ClientWhitNoPasswordDto> inscription(@RequestBody @Valid ClientInscriptionDto clientInscriptionDto) throws ClientException {
        return new ResponseEntity<>(clientService.Inscription(clientInscriptionDto), HttpStatus.OK);
    }

    @GetMapping("/{username}")
    public ResponseEntity<ClientWhitNoPasswordDto> findByUserName(@PathVariable String username) throws ClientException {
        return new ResponseEntity<>(clientService.findByUserName(username), HttpStatus.OK);
    }

    @DeleteMapping("/{username}")
    public void deleteByUserName(@PathVariable String username) throws ClientException {
        clientService.deleteByUserName(username);
    }

    @PutMapping("/{username}")
    public void updateClient(@RequestBody  @Valid ClientWhitNoPasswordDto client, @PathVariable String username) throws ClientException {
        clientService.update(username , client);
    }


}
