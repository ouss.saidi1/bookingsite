package com.example.demo.Controller;
import com.example.demo.dto.HotelDto;
import com.example.demo.exception.hotel.HotelException;
import com.example.demo.service.HotelService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/hotel")
@CrossOrigin(origins = "${front.end.url}")
@AllArgsConstructor
public class HotelController
{
    private HotelService hotelService;
    @GetMapping
    public ResponseEntity<List<HotelDto>>getAll() throws HotelException {
        return new ResponseEntity<>(hotelService.findAll(),HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<HotelDto> ajouter(@RequestBody  HotelDto hotelDto ) throws HotelException {
        return new ResponseEntity<>(hotelService.ajouter(hotelDto), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<HotelDto> findByUserName(@PathVariable Long id) throws HotelException {
        return new ResponseEntity<>(hotelService.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteHotel(@PathVariable Long id) throws HotelException {
        hotelService.deleteHotel(id);
    }
    @PutMapping("/{id}")
    public void updateHotel(@RequestBody HotelDto hotelDto, @PathVariable Long id) throws HotelException {
        hotelService.update(id ,hotelDto );
    }
}
