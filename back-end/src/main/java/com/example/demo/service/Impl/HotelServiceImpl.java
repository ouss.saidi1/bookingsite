package com.example.demo.service.Impl;

import com.example.demo.Utils.mapper.HotelMapper;
import com.example.demo.dto.HotelDto;
import com.example.demo.exception.client.ClientException;
import com.example.demo.exception.client.ExceptionMessage;
import com.example.demo.exception.hotel.HotelException;
import com.example.demo.exception.hotel.HotelExceptionMessage;
import com.example.demo.model.Client;
import com.example.demo.model.Hotel;
import com.example.demo.repository.HotelRepository;
import com.example.demo.service.HotelService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class HotelServiceImpl implements HotelService {

    private HotelRepository hotelRepository;
    private HotelMapper hotelMapper;
    @Override
    public HotelDto ajouter(HotelDto hotelDto) {

        Hotel toSave = hotelMapper.asHotel(hotelDto);
        hotelRepository.save(toSave);
        return hotelMapper.asHotelDto(toSave);
    }

    @Override
    public List<HotelDto> findAll() throws HotelException
    {
        List<Hotel> list = hotelRepository.findAll();
        if(list.isEmpty()) throw new HotelException(HotelExceptionMessage.EMPTY_HOTEL_LIST);
        return list.stream().map(hotel -> hotelMapper.asHotelDto(hotel)).collect(Collectors.toList());
    }

    @Override
    public HotelDto findById(Long id) throws HotelException
    {
        Optional<Hotel>hotel=hotelRepository.findById(id);
        if(hotel.isPresent())  return  hotelMapper.asHotelDto(hotel.get());
        else throw new HotelException(HotelExceptionMessage.HOTEl_NOT_FOUND);
    }

    @Override
    public void update(Long id, HotelDto hotelupdateDto) throws HotelException {
        Optional<Hotel> hotel = hotelRepository.findById(id);
        if(hotel.isPresent()) {
            Hotel toUpdate = hotel.get();
            toUpdate.setNom(hotelupdateDto.getNom());
            toUpdate.setNbEtoiles(hotelupdateDto.getNbEtoiles());
            toUpdate.setDescription(hotelupdateDto.getDescription());
            toUpdate.setVille(hotelupdateDto.getVille());
            toUpdate.setPays(hotelupdateDto.getPays());
            hotelRepository.save(toUpdate);
        }else throw new HotelException(HotelExceptionMessage.EMPTY_HOTEL_LIST);
    }
    @Override
    public void deleteHotel(Long id) throws HotelException {
        Optional<Hotel> hotel = hotelRepository.findById(id);
        if(hotel.isPresent())hotelRepository.delete(hotel.get());
        else throw new HotelException(HotelExceptionMessage.EMPTY_HOTEL_LIST);
    }


}
