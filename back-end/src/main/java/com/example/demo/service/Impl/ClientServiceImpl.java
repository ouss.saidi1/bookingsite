package com.example.demo.service.Impl;

import com.example.demo.Utils.mapper.ClientMapper;
import com.example.demo.dto.ClientInscriptionDto;
import com.example.demo.dto.ClientWhitNoPasswordDto;
import com.example.demo.exception.client.ExceptionMessage;
import com.example.demo.exception.client.ClientException;
import com.example.demo.model.Client;
import com.example.demo.repository.ClientRepository;
import com.example.demo.service.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.demo.exception.client.ExceptionMessage.ALlREADY_EXIST;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService  , UserDetailsService {

    private ClientRepository clientRepository;
    private ClientMapper clientMapper;
    private PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Client> client = clientRepository.findByUsername(username);

        if(client.isPresent()) {
            Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_CLIENT"));
            return new User(client.get().getUsername(),client.get().getPassword(),authorities);
        }else throw new UsernameNotFoundException("Not found");

    }

    @Override
    public ClientWhitNoPasswordDto Inscription(ClientInscriptionDto clientInscriptionDto) throws ClientException {
        Client toSave = clientMapper.asClient(clientInscriptionDto);
        if(clientRepository.existsByUsername(clientInscriptionDto.getUserName())) throw new ClientException(ExceptionMessage.ALlREADY_EXIST);
        else{
            toSave.setPassword(passwordEncoder.encode(toSave.getPassword()));
            clientRepository.save(toSave);
            return clientMapper.asClientWhitNoPasswordDto(toSave);
        }

    }

    @Override
    public List<ClientWhitNoPasswordDto> findAll() throws ClientException {
        List<Client> list = clientRepository.findAll();
        if(list.isEmpty()) throw new ClientException(ExceptionMessage.EMPTY_CLIENT_LIST);
        return list.stream().map(client -> clientMapper.asClientWhitNoPasswordDto(client)).collect(Collectors.toList());
    }

    @Override
    public ClientWhitNoPasswordDto findByUserName(String userName) throws ClientException {
        Optional<Client> client = clientRepository.findByUsername(userName);
        if(client.isPresent())  return clientMapper.asClientWhitNoPasswordDto(client.get());
        else throw new ClientException(ExceptionMessage.CLIENT_NOT_FOUND);
    }

    @Override
    public void deleteByUserName(String userName) throws ClientException {
        Optional<Client> client = clientRepository.findByUsername(userName);
        if(client.isPresent())  clientRepository.delete(client.get());
        else throw new ClientException(ExceptionMessage.CLIENT_NOT_FOUND);
    }

    @Override
    public void update(String username,  ClientWhitNoPasswordDto clientUpdateDto) throws ClientException {
        Optional<Client> client = clientRepository.findByUsername(username);
        if(client.isPresent()) {
            Client toUpdate = client.get();
            toUpdate.setBirthDate(clientUpdateDto.getBirthDate());
            toUpdate.setFullName(clientUpdateDto.getFullName());
            toUpdate.setEmail(clientUpdateDto.getEmail());
            toUpdate.setFullName(clientUpdateDto.getFullName());
            clientRepository.save(toUpdate);
        }else throw new ClientException(ExceptionMessage.CLIENT_NOT_FOUND);
    }



}
