package com.example.demo.service;

import com.example.demo.model.ImageModel;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
@Service
public interface ImageUploaderService
{
    ImageModel Convert(MultipartFile File);

}
