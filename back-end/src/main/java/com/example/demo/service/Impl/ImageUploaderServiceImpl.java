package com.example.demo.service.Impl;

import com.example.demo.model.ImageModel;
import com.example.demo.service.ImageUploaderService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
@Service
public class ImageUploaderServiceImpl implements ImageUploaderService {
    @Override
    public ImageModel Convert(MultipartFile File) {
        byte[] bytes = null;
        try {
            bytes = File.getBytes();
            return new ImageModel(Base64.encodeBase64String(bytes));
        } catch (
                IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }


}
