package com.example.demo.service;

import com.example.demo.dto.ClientInscriptionDto;
import com.example.demo.dto.ClientWhitNoPasswordDto;
import com.example.demo.exception.client.ClientException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ClientService {
    ClientWhitNoPasswordDto Inscription(ClientInscriptionDto clientInscriptionDto) throws ClientException;
    List<ClientWhitNoPasswordDto> findAll() throws ClientException;
    ClientWhitNoPasswordDto findByUserName(String userName) throws ClientException;
    void deleteByUserName(String userName) throws ClientException;
    void update(String username,  ClientWhitNoPasswordDto clientUpdateDto) throws ClientException;
}
