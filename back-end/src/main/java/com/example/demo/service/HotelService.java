package com.example.demo.service;


import com.example.demo.dto.HotelDto;

import com.example.demo.exception.hotel.HotelException;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public interface HotelService {
     HotelDto ajouter (HotelDto hotel );
     List<HotelDto> findAll() throws HotelException;
     HotelDto findById(Long id) throws HotelException;
     void update(Long id, HotelDto hotelupdateDto) throws HotelException;
      void deleteHotel(Long id) throws HotelException;


}
